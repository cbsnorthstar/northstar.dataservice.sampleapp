﻿namespace CheckFinder
{
    partial class Check_Finder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Check_Finder));
            this.LoadButton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.SiteIdBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TokenBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ItemGrid = new System.Windows.Forms.DataGridView();
            this.GridDataSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.GrossSalesBox = new System.Windows.Forms.TextBox();
            this.CheckNumberBox = new System.Windows.Forms.ComboBox();
            this.loadLabel = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ItemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDataSource)).BeginInit();
            this.SuspendLayout();
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(423, 245);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(130, 39);
            this.LoadButton.TabIndex = 0;
            this.LoadButton.Text = "Connect";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.Load_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(383, 173);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(227, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // SiteIdBox
            // 
            this.SiteIdBox.Location = new System.Drawing.Point(383, 104);
            this.SiteIdBox.Name = "SiteIdBox";
            this.SiteIdBox.Size = new System.Drawing.Size(227, 20);
            this.SiteIdBox.TabIndex = 2;
            this.SiteIdBox.Text = "C18BAB29-BEDA-4078-81D5-6791A2E17450";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Check Number";
            // 
            // TokenBox
            // 
            this.TokenBox.Location = new System.Drawing.Point(383, 33);
            this.TokenBox.Name = "TokenBox";
            this.TokenBox.Size = new System.Drawing.Size(227, 20);
            this.TokenBox.TabIndex = 5;
            this.TokenBox.Text = "wy39DVLSkXyG5ab2zrdCqtC65qLEqUeuk79EY7DgqTTUGPbFL63C9U2y";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(481, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Token";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(480, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Site ID";
            // 
            // ItemGrid
            // 
            this.ItemGrid.AllowUserToAddRows = false;
            this.ItemGrid.AllowUserToDeleteRows = false;
            this.ItemGrid.AutoGenerateColumns = false;
            this.ItemGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemGrid.DataSource = this.GridDataSource;
            this.ItemGrid.Location = new System.Drawing.Point(31, 304);
            this.ItemGrid.Name = "ItemGrid";
            this.ItemGrid.ReadOnly = true;
            this.ItemGrid.Size = new System.Drawing.Size(579, 139);
            this.ItemGrid.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(238, 464);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Gross Sales";
            // 
            // GrossSalesBox
            // 
            this.GrossSalesBox.Location = new System.Drawing.Point(307, 464);
            this.GrossSalesBox.Name = "GrossSalesBox";
            this.GrossSalesBox.Size = new System.Drawing.Size(100, 20);
            this.GrossSalesBox.TabIndex = 10;
            this.GrossSalesBox.Text = "$0.00";
            // 
            // CheckNumberBox
            // 
            this.CheckNumberBox.DataSource = this.GridDataSource;
            this.CheckNumberBox.DisplayMember = "CheckNumber";
            this.CheckNumberBox.FormattingEnabled = true;
            this.CheckNumberBox.Location = new System.Drawing.Point(32, 277);
            this.CheckNumberBox.Name = "CheckNumberBox";
            this.CheckNumberBox.Size = new System.Drawing.Size(169, 21);
            this.CheckNumberBox.TabIndex = 11;
            this.CheckNumberBox.ValueMember = "CheckNumber";
            // 
            // loadLabel
            // 
            this.loadLabel.AutoSize = true;
            this.loadLabel.Location = new System.Drawing.Point(519, 464);
            this.loadLabel.Name = "loadLabel";
            this.loadLabel.Size = new System.Drawing.Size(0, 13);
            this.loadLabel.TabIndex = 12;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(31, 104);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(346, 51);
            this.richTextBox1.TabIndex = 13;
            this.richTextBox1.Text = "Step 2: Obtain a SiteId. This ID specifies which server you want to connect to. T" +
    "o obtain a Site Id, please login to ECM and edit Site settings. Your site ID can" +
    " be viewed there.\n";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(31, 33);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(346, 51);
            this.richTextBox2.TabIndex = 14;
            this.richTextBox2.Text = "Step 1: Obtain an access token. This token is used to authorize your program to c" +
    "onnect to the data service. Please contact your Custom Business Solutions Repres" +
    "entative to obtain an access token.\n";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(31, 173);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(346, 51);
            this.richTextBox3.TabIndex = 15;
            this.richTextBox3.Text = "Step 3: Select the date on which you want data, and hit the Connect button. Your " +
    "data will populate the grid below.\n";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "1.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "2.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "3.";
            // 
            // Check_Finder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(636, 503);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.richTextBox3);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.loadLabel);
            this.Controls.Add(this.CheckNumberBox);
            this.Controls.Add(this.GrossSalesBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ItemGrid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TokenBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SiteIdBox);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.LoadButton);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Check_Finder";
            this.Text = "Check_Finder";
            ((System.ComponentModel.ISupportInitialize)(this.ItemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDataSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox SiteIdBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TokenBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView ItemGrid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox GrossSalesBox;
        private System.Windows.Forms.ComboBox CheckNumberBox;
        private System.Windows.Forms.Label loadLabel;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingSource GridDataSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn grossSalesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn siteObjectIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businessDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberInPartyDataGridViewTextBoxColumn;
    }
}