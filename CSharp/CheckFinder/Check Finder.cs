﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Services.Client;
using CheckFinder.DataService;
using System.Configuration;

namespace CheckFinder
{
    public partial class Check_Finder : Form
    {
        public Check_Finder()
        {
            InitializeComponent();
            this.ItemGrid.AutoGenerateColumns = true;
        }

        //Load checks when the connect button is pressed
        private void Load_Click(object sender, EventArgs e)
        {
            loadLabel.Text = "Loading Data....";
            if (dateTimePicker1.Value != null)
            {
                //Loads checks for business day selected in the TimePicker
                LoadChecks(dateTimePicker1.Value);
                loadLabel.Text = "Done!";
            }
        }

        private void LoadChecks(DateTime dt)
        {
            try
            {
                //Parse the SiteID from the winForm into GUID format
                Guid siteId = Guid.Parse(SiteIdBox.Text.Trim());



                // Creates a new Uri Address. this is the Service Url for the data service.
                Uri address = new Uri(ConfigurationManager.AppSettings["ReportService.SalesData"]);

                // Instantiates the GOPS context for Uri address defined above
                var context = new GOPS(address);

                // By default, a DataServiceContext will throw if the OData source adds new fields.
                // Setting this to "true" is mandatory; otherwise, this code would break when CBS adds new fields.
                context.IgnoreMissingProperties = true;

                // Add an event handler that will authorize the program to connect to the data service using the access token 
                context.SendingRequest2 += AddAuthorizationHeader;


                // Define a LINQ query that returns checks for specified date and Site
                // To view the Data object hiarchy, explore "Reference.cs" under the DataService reference  

                // Use .Expand() to populate the child entities.
                // The example below will populate the checks' ItemSales entities,
                // as well as the ItemSaleComponents inside each ItemSale.
                var checksQuery = from o in context.Checks.Expand("ItemSales/ItemSaleComponents")
                                  where o.Site_ObjectId == siteId && o.BusinessDate == dt.Date
                                  select o;


                // Create a DataServiceCollection<T> based on 
                // execution of the LINQ query for Orders.
                DataServiceCollection<Check> paymentChecks = new DataServiceCollection<Check>(checksQuery);

                // The server may have a maximum number of records it will send.
                // For example, only the first 100 records may come down in the first query.
                // This while loop loads additional pages of data if necessary.
                // OData documentation: https://msdn.microsoft.com/en-us/library/dd942121.aspx
                // WCF documentation:   https://msdn.microsoft.com/en-us/library/ee358709%28v=vs.110%29.aspx
                while (paymentChecks.Continuation != null)
                {
                    var nextPage = context.Execute(paymentChecks.Continuation);
                    paymentChecks.Load(nextPage);
                }

                // Make sure the request returned results. 
                // If no records are returned, display error message
                if (paymentChecks.Count == 0)
                {
                    MessageBox.Show("Request yielded zero results!");
                }
                else
                {
                    // Bind the response to the Grid/Drop down box.
                    ItemGrid.DataSource = paymentChecks;
                    CheckNumberBox.DataSource = paymentChecks;

                    // Display gross sales for day specified
                    GrossSalesBox.Text = checksQuery.ToList().Sum(x => x.GrossSales).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void AddAuthorizationHeader(object sender, SendingRequest2EventArgs e)
        {
            string accessToken = TokenBox.Text.Trim();
            e.RequestMessage.SetHeader("Authorization", "AccessToken=" + accessToken);
        }
    }
}
