﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Services.Client;
using System.Data.Services.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeList.ConfigurationData;

namespace EmployeeList
{
    public partial class EmployeeListForm : Form
    {
        public string AccessToken { get { return accessTokenTextBox.Text.Trim(); } }
        public Uri ConfigurationServiceUri { get { return new Uri(configurationDataUrlTextBox.Text.Trim()); } }

        public EmployeeListForm()
        {
            InitializeComponent();
        }

        void getEmployeesButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Query Data Service for employees
                DataServiceCollection<Employee> employeeCollection = GetActiveEmployees();

                // Update the DataGridView with the results
                var employeeJobRecords = ConvertToEmployeeJobRecords(employeeCollection);
                employeeJobRecordBindingSource.DataSource = employeeJobRecords;

                // If no records are returned, display an error message
                if (employeeCollection.Count == 0)
                {
                    MessageBox.Show("No employees are assigned jobs.");
                }
            }
            catch (Exception ex)
            {
                employeeJobRecordBindingSource.Clear();
                MessageBox.Show(ex.ToString());
            }
        }


        DataServiceCollection<Employee> GetActiveEmployees()
        {
            // A ConfigurationDataContext represents the OData endpoint
            ConfigurationDataContext context = new ConfigurationDataContext(ConfigurationServiceUri);

            // This prevents the Microsoft OData client from throwing an exception
            // if a new property is added on the server.
            context.IgnoreMissingProperties = true;

            // This event handler adds the Authorization header to each request.
            context.SendingRequest2 += AddAuthorizationHeader;


            // This LINQ query finds all employees who have not been disabled.
            // To view the Data object hiarchy, explore "Reference.cs" under the ConfigurationData reference  
            var employeesQuery = from emp in context.Employees.Expand("EmployeeJobRates/Job")
                                 where emp.Disabled == false
                                 orderby emp.FullName
                                 select emp;


            // Create a DataServiceCollection<T> which will execute the LINQ query
            DataServiceCollection<Employee> employeeCollection = new DataServiceCollection<Employee>(employeesQuery);

            // The server may have a maximum number of records it will send.
            // For example, only the first 100 records may come down in the first query.
            // This while loop loads additional pages of data if necessary.
            // OData documentation: https://msdn.microsoft.com/en-us/library/dd942121.aspx
            // WCF documentation:   https://msdn.microsoft.com/en-us/library/ee358709%28v=vs.110%29.aspx
            while (employeeCollection.Continuation != null)
            {
                var nextPage = context.Execute(employeeCollection.Continuation);
                employeeCollection.Load(nextPage);
            }

            return employeeCollection;
        }

        void AddAuthorizationHeader(object sender, SendingRequest2EventArgs e)
        {
            e.RequestMessage.SetHeader("Authorization", "AccessToken=" + AccessToken);
        }

        List<EmployeeJobRecord> ConvertToEmployeeJobRecords(IEnumerable<Employee> employees)
        {
            return (from emp in employees
                    from jobRate in emp.EmployeeJobRates
                    select new EmployeeJobRecord
                    {
                        EmployeeId = emp.EmployeeId,
                        FullName = emp.FullName,
                        JobName = jobRate.Job.Name,
                        PayRate = jobRate.Rate,
                    })
                    .ToList();

        }
    }

    public class EmployeeJobRecord
    {
        public Guid EmployeeId { get; set; }
        public string FullName { get; set; }
        public string JobName { get; set; }
        public decimal PayRate { get; set; }
    }
}
