﻿namespace EmployeeList
{
    partial class EmployeeListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.accessTokenTextBox = new System.Windows.Forms.TextBox();
            this.configurationDataUrlTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.employeeResultGridView = new System.Windows.Forms.DataGridView();
            this.getEmployeesButton = new System.Windows.Forms.Button();
            this.employeeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payRateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employeeJobRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.employeeResultGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeJobRecordBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Access Token";
            // 
            // accessTokenTextBox
            // 
            this.accessTokenTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.accessTokenTextBox.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accessTokenTextBox.Location = new System.Drawing.Point(230, 48);
            this.accessTokenTextBox.Name = "accessTokenTextBox";
            this.accessTokenTextBox.Size = new System.Drawing.Size(991, 30);
            this.accessTokenTextBox.TabIndex = 2;
            this.accessTokenTextBox.Text = "wy39DVLSkXyG5ab2zrdCqtC65qLEqUeuk79EY7DgqTTUGPbFL63C9U2y";
            // 
            // configurationDataUrlTextBox
            // 
            this.configurationDataUrlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configurationDataUrlTextBox.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationDataUrlTextBox.Location = new System.Drawing.Point(230, 12);
            this.configurationDataUrlTextBox.Name = "configurationDataUrlTextBox";
            this.configurationDataUrlTextBox.Size = new System.Drawing.Size(991, 30);
            this.configurationDataUrlTextBox.TabIndex = 4;
            this.configurationDataUrlTextBox.Text = "https://nsoewebservices.cbsnorthstar.com/ReportService/ConfigurationData.svc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "URL";
            // 
            // employeeResultGridView
            // 
            this.employeeResultGridView.AllowUserToAddRows = false;
            this.employeeResultGridView.AllowUserToDeleteRows = false;
            this.employeeResultGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.employeeResultGridView.AutoGenerateColumns = false;
            this.employeeResultGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employeeResultGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.employeeIdDataGridViewTextBoxColumn,
            this.fullNameDataGridViewTextBoxColumn,
            this.jobNameDataGridViewTextBoxColumn,
            this.payRateDataGridViewTextBoxColumn});
            this.employeeResultGridView.DataSource = this.employeeJobRecordBindingSource;
            this.employeeResultGridView.Location = new System.Drawing.Point(12, 256);
            this.employeeResultGridView.Name = "employeeResultGridView";
            this.employeeResultGridView.ReadOnly = true;
            this.employeeResultGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.employeeResultGridView.RowTemplate.Height = 28;
            this.employeeResultGridView.Size = new System.Drawing.Size(1209, 818);
            this.employeeResultGridView.TabIndex = 6;
            // 
            // getEmployeesButton
            // 
            this.getEmployeesButton.Location = new System.Drawing.Point(230, 84);
            this.getEmployeesButton.Name = "getEmployeesButton";
            this.getEmployeesButton.Size = new System.Drawing.Size(200, 63);
            this.getEmployeesButton.TabIndex = 7;
            this.getEmployeesButton.Text = "Get Employees";
            this.getEmployeesButton.UseVisualStyleBackColor = true;
            this.getEmployeesButton.Click += new System.EventHandler(this.getEmployeesButton_Click);
            // 
            // employeeIdDataGridViewTextBoxColumn
            // 
            this.employeeIdDataGridViewTextBoxColumn.DataPropertyName = "EmployeeId";
            this.employeeIdDataGridViewTextBoxColumn.HeaderText = "EmployeeId";
            this.employeeIdDataGridViewTextBoxColumn.Name = "employeeIdDataGridViewTextBoxColumn";
            this.employeeIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fullNameDataGridViewTextBoxColumn
            // 
            this.fullNameDataGridViewTextBoxColumn.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn.HeaderText = "FullName";
            this.fullNameDataGridViewTextBoxColumn.Name = "fullNameDataGridViewTextBoxColumn";
            this.fullNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jobNameDataGridViewTextBoxColumn
            // 
            this.jobNameDataGridViewTextBoxColumn.DataPropertyName = "JobName";
            this.jobNameDataGridViewTextBoxColumn.HeaderText = "JobName";
            this.jobNameDataGridViewTextBoxColumn.Name = "jobNameDataGridViewTextBoxColumn";
            this.jobNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // payRateDataGridViewTextBoxColumn
            // 
            this.payRateDataGridViewTextBoxColumn.DataPropertyName = "PayRate";
            this.payRateDataGridViewTextBoxColumn.HeaderText = "PayRate";
            this.payRateDataGridViewTextBoxColumn.Name = "payRateDataGridViewTextBoxColumn";
            this.payRateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // employeeJobRecordBindingSource
            // 
            this.employeeJobRecordBindingSource.DataSource = typeof(EmployeeList.EmployeeJobRecord);
            // 
            // EmployeeListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 1086);
            this.Controls.Add(this.getEmployeesButton);
            this.Controls.Add(this.employeeResultGridView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.configurationDataUrlTextBox);
            this.Controls.Add(this.accessTokenTextBox);
            this.Controls.Add(this.label1);
            this.Name = "EmployeeListForm";
            this.Text = "Employee List";
            ((System.ComponentModel.ISupportInitialize)(this.employeeResultGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeJobRecordBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox accessTokenTextBox;
        private System.Windows.Forms.TextBox configurationDataUrlTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView employeeResultGridView;
        private System.Windows.Forms.Button getEmployeesButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn employeeIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payRateDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource employeeJobRecordBindingSource;
    }
}

