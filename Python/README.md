Data Service Python Sample App
===============================

This is a simple command line app which queries NorthStar Data Service for
sales data.

The file `nscmd` shows how to build a query and iterate over the results.


## Getting started

    pip install -r requirements.txt
    export DATASERVICE_TOKEN='<your token here>'
    ./nscmd-with-fluentquery


## Why two commands?

The underlying library for calling OData is [Pyslet][pyslet]. The object
`dataservice.Client` is an extension of `pyslet.odata2.client.Client`, so you
can use it according to their documentation. See the file `nscmd-with-pyslet`
for an example.

If you prefer a more fluent-style interface, the command
`nscmd-with-fluentquery` demonstrates using a thin wrapper around pyslet
which lets you chain method calls to build the query.

The fluent interface isn't quite right. Some of pyslet's methods like
`OpenCollection()` leak out, and the query methods like `where()` are
modifying the query in-place (you have to call `client[table_name]` again to
make a new query).

Use whichever feels most comfortable to you.

[pyslet]: http://pyslet.readthedocs.org/en/pyslet-0.5.20140801/odatav2_client.html 


## Query syntax

The `where()` sntax matches the undelrying [OData `$filter`
syntax][filter-syntax]. Some examples:

* `Price gt 4`: Price is greater than 4.00
* `BusinessDate eq datetime'2015-02-15T00:00:00'`: BusinessDate equals
  2/15/2015 at midnight
* `startswith(ItemName, "special")`: ItemName begins with the string
  "special"

Multiple calls to `where()` will be joined together with "AND".

[filter-syntax]: http://www.odata.org/documentation/odata-version-3-0/odata-version-3-0-core-protocol#queryingcollections


## License

Licensed under the MIT license.

Copyright (c) 2015 Custom Business Solutions, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
