from pyslet.odata2 import core
from pyslet.odata2 import client
import fluentquery as fq

OrigClient = client.Client

class Client(client.Client):

    def __init__(self, url, token):
        self.token = token
        super(Client, self).__init__(url)

    def queue_request(self, request, timeout=60):
        if not request.has_header("Authorization"):
            request.set_header("Authorization", "AccessToken=" + self.token)
        super(OrigClient, self).queue_request(request, timeout)

    def __getitem__(self, name):
        coll = self.feeds[name].OpenCollection()
        return fq.FluentQuery(coll)

# Monkey-patch the pyslet OData client
client.Client = Client
