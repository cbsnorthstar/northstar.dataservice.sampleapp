from pyslet.odata2 import core

class FluentQuery(object):
    def __init__(self, collection):
        self.collection = collection
        self.takenum = 1000
        self.skipnum = 0
        self.orderbystr =  None
        self.whereclause = None
        self.expansion = None

    def where(self, expr):
        if self.whereclause == None:
            self.whereclause = expr
        else:
            self.whereclause += " and " + expr
        return self

    def take(self, num):
        self.takenum = num
        return self

    def skip(self, num):
        self.skipnum = num
        return self

    def expand(self, expansion):
        def merge_dicts(existing, new):
            if existing == None: return new
            for key in new.keys():
                if existing.has_key(key):
                    existing[key] = merge_dicts(existing[key], new[key])
                else:
                    existing[key] = new[key]
            return existing

        terms = expansion.split(".")
        terms.reverse()
        d = None
        for term in terms:
            d = { term: d }

        self.expansion = merge_dicts(self.expansion, d)
        return self

    def orderby(self, expr):
        self.orderbystr = expr
        return self

    def execute(self):
        if self.whereclause != None:
            clause = core.CommonExpression.from_str(self.whereclause)
            self.collection.set_filter(clause)
        if self.orderbystr != None:
            order = core.CommonExpression.OrderByFromString(self.orderbystr)
            self.collection.set_orderby(order)
        self.collection.Expand(self.expansion)
        self.collection.set_page(self.takenum, self.skipnum)
        return self.collection.iterpage()
