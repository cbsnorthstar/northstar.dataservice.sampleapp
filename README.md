NorthStar Data Service Sample Apps
==================================

Welcome!

This repository contains sample applications for querying data from NorthStar
Order Entry.

Data is available via an [OData][odata] endpoint:

    http://nsoewebservices.cbsnorthstar.com/ReportService/SalesData.svc

Queries are made using the OData standard. There are [client libraries
available][odata-clients] available for many languages, including:

* .NET
    * [Microsoft WCF Data Services Client](https://www.nuget.org/packages/Microsoft.Data.Services.Client/)

* Python
    * [Psylet](http://www.pyslet.org/)

* Java
    * [Apache Olingo](http://olingo.apache.org/)
    * [ODataJClient](https://github.com/MSOpenTech/ODataJClient)
    

[odata]: http://www.odata.org/
[odata-clients]: http://www.odata.org/libraries/
